## Docker Control Panel

This repo will run an instance of Nerva in a docker container and provide a web based UI for  
mining, wallet, explorer and api functions. The only dependency to run this is docker itself

### Install

`docker pull nerva/node`  
`docker run -i --privileged --name "nerva" --publish 17500:80 --publish 17565:17565 --publish 17566:17566 -t nerva/node:latest /bin/bash`

You will then have a docker prompt with nervad running in the background.  
You can issue all the same commands to nervad as you would if it was running by prefixing the command with `nervad`, such as `nervad status`

If you do not require the prompt, you can simply type `exit` to exit the container

### Reconnecting to a docker container

`docker start nerva`  
`docker exec -it nerva /bin/bash`
