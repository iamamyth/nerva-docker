<style>
    h4 {
        color:#434343;
    }
</style>
<div class="well bs-component">
    <form class="form-horizontal">
        <h4><i class="fa fa-info fa-fw"></i> Miner Status</h4>
        <div class="panel-body">
            <fieldset>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="miner_address">Address:</label>
                        <span id="miner_address" class="value"></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="miner_speed">Hashrate:</label>
                        <span id="miner_speed" class="value"></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="miner_threads">Threads:</label>
                        <span id="miner_threads" class="value"></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-default" type="button" id="miner_toggle">Start Miner</button>
                    </div>
                </div>
            </fieldset>
        </div>
    </form>
</div>

<div class="well bs-component">
    <form class="form-horizontal">
        <h4><i class="fa fa-bar-chart fa-fw"></i> Node Hashrate</h4>
        <fieldset>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="panel-body chart-wrapper">
                        <canvas id="difficultyChart" height="210"></canvas>
                    </div>
                </div>
            </div> 
        </fieldset>
    </form>
</div>

<div class="well bs-component">
	<form class="form-horizontal" id="miner_settings">
		<h4><i class="fa fa-cog fa-fw"></i> Miner Settings</h4>
		<fieldset>
			<div class="col-md-12">
				<div class="form-group">
					<label for="miner_address" style="margin-bottom: 10px;">Address</label>
					<input class="form-control" placeholder="NERVA Address" id="miner_settings_address">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="miner_threads" style="margin-bottom: 10px;">Threads</label>
					<input type="number" min="0" max="100" class="form-control" id="miner_settings_threads">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<button class="btn btn-default" type="button" id="miner_save">Save</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>

<script>
    var currentPage;
    var minerActive = false;

    var xhrGetHashRate, xhrGetMinerStats, xhrToggleMiner, xhrMinerSave, xhrGetMinerSettings;

    function displayChart() {
        var ctx = document.getElementById("difficultyChart");
        var chartData = {
            datasets: [{
                data: hashRates,
                yAxisID: "diff",
                label: "Hashrate",
                backgroundColor: "rgba(220,220,220,0.2)",
                borderColor: '#2FA4E7',
                borderWidth: 2,
                pointColor: "#2FA4E7",
                pointBorderColor: "#2FA4E7",
                pointHighlightFill: "#2FA4E7",
                pointBackgroundColor: "#2FA4E7",
                pointBorderWidth: 2,
                pointRadius: 1,
                pointHoverRadius: 3,
                pointHitRadius: 20,
                type: 'line'
            }]
        };
        var options = {
            responsive: true,
            maintainAspectRatio: false,
            elements: {
                line: {
                    tension: 0
                }
            },
            title: {
                display: false
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    type: 'linear',
                    id: "diff",
                    distribution: 'linear',
                    position: 'left',
                    scaleLabel: {
                        display: true,
                        labelString: 'Hash Rate'
                    },
                    gridLines: {
                        lineWidth: 1,
                        display: true
                    },
                    ticks: {
                        fontSize: 9,
                        display: true
                    },
                    display: true
                }],
                xAxes: [
                    {
                        type: 'linear',
                        distribution: 'linear',
                        position: 'left',
                        scaleLabel: {
                            display: false,
                            labelString: 'Time'
                        },
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            fontSize: 9,
                            display: false,
                            stepSize: 1
                        },
                        display: true
                    }]
            }
        };

        if (!chartData || !options)
            return;

        diffChart = new Chart(ctx, {
            type: 'line',
            data: chartData,
            options: options
        });
    }

    currentPage = {
        destroy: function () {
            if (xhrGetHashRate) xhrGetHashRate.abort();
            if (xhrGetMinerStats) xhrGetMinerStats.abort();
            if (xhrToggleMiner) xhrToggleMiner.abort();
            if (xhrMinerSave) xhrMinerSave.abort();
            if (xhrGetMinerSettings) xhrGetMinerSettings.abort();
        },
        init: function () {
            updateMinerSettings();
            displayChart();
            updateChart();
            updateMinerInfo();
            currentPage.update();
        },
        update: function () {
        }
    };

    $("#miner_save").click(function () {
		xhrMinerSave = $.ajax({
			url: './content/php/save_miner_settings.php?address=' + 
                $("#miner_settings_address").val().trim() + '&threads=' + $("#miner_settings_threads").val().trim(),
			dataType: 'json',
			cache: 'false',
			success: function (data) {
				if (data == "OK") {
                    updateMinerInfo();
					alertSuccess("Miner settings saved");
				} else {
					alertError("Miner settings not saved");
				}
			}
		});
	});

    $("#miner_toggle").click(function()
    {
        if (minerActive)
        {
            xhrToggleMiner = $.ajax({
                url: './api/daemon/stop_mining',
                dataType: 'json',
                cache: 'false',
                success: function (data) {
                    updateMinerInfo();
                }
            });
        } else {
            
            xhrGetMinerSettings = $.ajax({
                url: './content/php/get_miner_settings.php',
                dataType: 'json',
                cache: 'false',
                success: function (data) {
                    if (data.status == "OK") {
                        a = data.address;
                        t = data.threads;
                        xhrToggleMiner = $.ajax({
                            url: './api/daemon/start_mining/?address=' + a + '&threads=' + t,
                            dataType: 'json',
                            cache: 'false',
                            success: function (data) {
                                if (data.status == "OK") {
                                    updateMinerInfo();
                                } else {
                                    alertError(data.status);
                                }
                            }
                        });
                    } else {
                        alertError(data.status);
                    }
                }
            });
        }
    });

    function updateMinerSettings()
    {
        xhrGetMinerSettings = $.ajax({
            url: './content/php/get_miner_settings.php',
            dataType: 'json',
            cache: 'false',
            success: function (data) {
                $("#miner_settings_address").val(data.address);
                $("#miner_settings_threads").val(data.threads);
            }
        });
    }

    function updateMinerInfo()
    {
        xhrGetMinerStats = $.ajax({
            url: "./api/daemon/mining_status/",
            dataType: 'json',
            cache: 'false'
        }).done(function (data, success) {
            if (data.active) {
                updateText("miner_address", data.address);
                updateText("miner_speed", getReadableHashRateString(data.speed));
                updateText("miner_threads", data.threads_count);
                $("#miner_toggle").text("Stop Mining");
                minerActive = true;
            }
            else {
                updateText("miner_address", "Not mining");
                updateText("miner_speed", "Not mining");
                updateText("miner_threads", "Not mining");
                $("#miner_toggle").text("Start Mining");
                minerActive = false;
            }

            currentPage.update();
        }).always(function () {
            setTimeout(function () {
                updateMinerInfo();
            }, refreshDelay / 10);
        });
    }

    function updateChart()
    {
        xhrGetHashRate = $.ajax({
            url: "./api/daemon/mining_status/",
            dataType: 'json',
            cache: 'false'
        }).done(function (data, success) {
            if (data.active) {
                hashRates.push({
                    x: 0,
                    y: data.speed
                });
            }

            if (diffChart) {
                if (hashRates.length > 120)
                    hashRates.splice(0, hashRates.length - 120);
                for (var i = 0; i < hashRates.length; i++)
                    hashRates[i].x = i;
                diffChart.update();
            }

            currentPage.update();
        }).always(function () {
            setTimeout(function () {
                updateChart();
            }, refreshDelay);
        });
    }

//# sourceURL=./pages/miner.php
</script>