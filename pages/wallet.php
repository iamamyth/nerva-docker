<style>
    h4 {
        color:#434343;
    }
</style>
<div class="row well bs-component">
    <form class="form-horizontal">
        <fieldset>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="wallet_address">Address</label>
                    <span id="wallet_address" class="value"></span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="wallet_locked_balance">Locked Balance</label>
                    <span id="wallet_locked_balance" class="value"></span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="wallet_unlocked_balance">Unlocked Balance</label>
                    <span id="wallet_unlocked_balance" class="value"></span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <button class="btn btn-default" type="button" id="miner_toggle">
                        <span class="fa fa-search"></span> Start Miner
                    </button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<div class="row well bs-component">
    <form class="form-horizontal">
        <fieldset>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="difficultyChart">Wallet Balance</label>
                    <div class="panel-body chart-wrapper">
                        <canvas id="difficultyChart" height="210"></canvas>
                    </div>
                </div>
            </div>  
        </fieldset>
    </form>
</div>

<script>
    var currentPage;

    function displayChart() {
        var ctx = document.getElementById("difficultyChart");
        var chartData = {
            datasets: [{
                data: balances,
                yAxisID: "diff",
                label: "Balance",
                backgroundColor: "rgba(220,220,220,0.2)",
                borderColor: '#2FA4E7',
                borderWidth: 2,
                pointColor: "#2FA4E7",
                pointBorderColor: "#2FA4E7",
                pointHighlightFill: "#2FA4E7",
                pointBackgroundColor: "#2FA4E7",
                pointBorderWidth: 2,
                pointRadius: 1,
                pointHoverRadius: 3,
                pointHitRadius: 20,
                type: 'line'
            }]
        };
        var options = {
            responsive: true,
            maintainAspectRatio: false,
            elements: {
                line: {
                    tension: 0
                }
            },
            title: {
                display: false
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    type: 'linear',
                    id: "diff",
                    distribution: 'linear',
                    position: 'left',
                    scaleLabel: {
                        display: true,
                        labelString: 'Hash Rate'
                    },
                    gridLines: {
                        lineWidth: 1,
                        display: true
                    },
                    ticks: {
                        fontSize: 9,
                        display: true
                    },
                    display: true
                }],
                xAxes: [
                    {
                        type: 'linear',
                        distribution: 'linear',
                        position: 'left',
                        scaleLabel: {
                            display: false,
                            labelString: 'Time'
                        },
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            fontSize: 9,
                            display: false,
                            stepSize: 1
                        },
                        display: true
                    }]
            }
        };

        if (!chartData || !options)
            return;

        diffChart = new Chart(ctx, {
            type: 'line',
            data: chartData,
            options: options
        });
    }

    currentPage = {
        destroy: function () {
        },
        init: function () {
            displayChart();
            updateChart();
            currentPage.update();
        },
        update: function () {
        }
    };

    function updateChart() {

        xhrGetHashRate = $.ajax({
            url: "./content/php/get_sorted_transfers.php?min_height=" + lastBalanceCheckHeight,
            dataType: 'json',
            cache: 'false'
        }).done(function (data, success) {
            if (data) {
                lastBalanceCheckHeight = data.last;

                var b = data.balances;

                balances.push(b);

                for (var i = 0; i < balances.length; i++)
                    balances[i].x = i;

                updateText("wallet_address", "Not implemented");
                updateText("wallet_locked_balance", "Not implemented");
                updateText("wallet_unlocked_balance", "Not implemented");
            }
            else {
                balances = [];
                updateText("wallet_address", "Wallet closed");
                updateText("wallet_locked_balance", "0");
                updateText("wallet_unlocked_balance", "0")
            }

            if (diffChart) {
                diffChart.update();
            }

            currentPage.update();
        }).always(function () {
            setTimeout(function () {
                updateChart();
            }, refreshDelay);
        });
    }

//# sourceURL=./pages/wallet.php
</script>