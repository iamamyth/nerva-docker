<?php 
require_once('../../api/lib/config.php');
require_once('../../api/lib/helper.php');

$min_height = 0;

if (isset($_GET['min_height'])) {
    $account_index = $_GET['min_height'];
}

$params = array(
    'account_index' => 0,
    'in' => true,
    'out' => true,
    'pending' => true,
    'pool' => false,
    'min_height' => $min_height
);

$json = send_json_rpc_request(HOST, WALLET_PORT, 'get_transfers', $params);
$rpc_data = json_decode($json);

$tx_tmp = array();
$tx_in = array();
$tx_out = array();
$tx_final = array();

foreach($rpc_data->in as $i) {
    if (array_key_exists($i->height, $tx_tmp)) {
        $tx_tmp[$i->height][0] += ($i->amount / 1000000000000);
    }
    else {
        $tx_tmp += [ $i->height, ($i->amount / 1000000000000) ];
    }

    $tx_in += [ $i->height, array(
        'height' => $i->height,
        'amount' => ($i->amount / 1000000000000),
        'address' => $i->address,
        'id' => $i->txid,
        'ts' => $i->timestamp
    )];
}

foreach($rpc_data->out as $i) {
    if (array_key_exists($i->height, $tx_tmp)) {
        $tx_tmp[$i->height][0] -= ($i->amount / 1000000000000);
    }
    else {
        $tx_tmp = [ $i->height, -($i->amount / 1000000000000) ];
    }

    $tx_out += [ $i->height, array(
        'height' => $i->height,
        'amount' => ($i->amount / 1000000000000),
        'address' => $i->address,
        'id' => $i->txid,
        'ts' => $i->timestamp
    )];
}

ksort($tx_tmp);

$total = 0;
foreach($tx_tmp as $key => $val) {
    $tx_final[] = $total; 
    $total += $val;
}

$ret_data = array(
    'in' => $tx_in,
    'out' => $tx_out,
    'balance' => $tx_final,
    'last' => array_key_last(tx_tmp)
);

echo json_encode($ret_data);

?>