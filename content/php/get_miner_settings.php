<?php
    require_once('../miner_config.php');
    
    $result = array (
        "status" => "OK",
        "address" => MINER_ADDRESS,
        "threads" => MINER_THREADS
    );

    echo json_encode($result);
?>