<?php
    require_once('../../api/lib/config.php');
    require_once('../../api/lib/helper.php');
    require_once('../miner_config.php');

    $address = $_GET['address'];
    $threads = $_GET['threads'];

    $need_restart = false;
    if ($address != MINER_ADDRESS || $threads != MINER_THREADS) {
        $need_restart = true;
    }

    $content = "<?php\n" .
    "define(MINER_ADDRESS, '" . $address . "');\n" .
    "define(MINER_THREADS, '" . $threads . "');\n" .
    "?>";

    file_put_contents("../miner_config.php", $content);

    //if the file content has changed, we need to restart the miner if it is active
    // So first thing we do is check the miner status with mining_status
    // If active == true, we call stop_mining, followed by start_mining with our new settings

    if ($need_restart) {
        
        $obj = send_rpc_request(HOST, DAEMON_PORT, 'mining_status', null);
        $json = json_decode($obj);

        if ($json->active)
        {
            send_rpc_request(HOST, DAEMON_PORT, 'stop_mining', null);

            $params = array(
                "miner_address" => $address,
                "do_background_mining" => false,
                "ignore_battery" => true,
                "threads_count" => $threads
            );

            $obj = send_rpc_request(HOST, DAEMON_PORT, 'start_mining', $params);
        }
    }

    echo "OK";
?>