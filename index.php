<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
	<link rel="shortcut icon" href="./content/favicon.ico">
	<link rel="icon" type="image/icon" href="./content/favicon.ico" >
    <title>NERVA Node Monitor</title>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>
    <link href="./content/css/themes/nerva/style.css" rel="stylesheet" id="theme_nerva">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet" type="text/css">
	<script src="./content/js/Chart.bundle.min.js"></script>
	<script src="./content/js/sorttable.js"></script>
	<script src="./content/js/config.js"></script>
	<script src="./content/js/paperwallet/languages.js"></script>
	<script src="./content/js/paperwallet/paperwallet.js"></script>
	<script src="./content/js/nacl-fast-cn.js"></script>
	<script src="./content/js/cn_helper.js"></script>
</head>
<body>
<script>

	var blockchainExplorer 	= "./?hash={id}#blockchain_block";
	var transactionExplorer = "./?hash={id}#blockchain_transaction";
	var paymentIdExplorer 	= "./?hash={id}#blockchain_payment_id";

	var hashRates = [];
	var balances = [];
	var lastBalanceCheckHeight = 0;
	
    function getTransactionUrl(id) {
        return transactionExplorer.replace('{symbol}', symbol.toLowerCase()).replace('{id}', id);
    }
    
    $.fn.update = function(txt){
        var el = this[0];
        if (el.textContent !== txt)
            el.textContent = txt;
        return this;
    };

    function updateTextClasses(className, text){
        var els = document.getElementsByClassName(className);
        for (var i = 0; i < els.length; i++){
            var el = els[i];
            if (el.textContent !== text)
                el.textContent = text;
        }
    }

    function updateText(elementId, text){
		
		var el = document.getElementById(elementId);

		if (!el)
			return;

        if (el.textContent !== text){
            el.textContent = text;
        }
        return el;
    }

    function updateTextLinkable(elementId, text){
        var el = document.getElementById(elementId);
        if (el.innerHTML !== text){
            el.innerHTML = text;
        }
        return el;
    }

    var currentPage;
    var lastStats;
    var nodeStatus;

    function getReadableHashRateString(hashrate){
        var i = 0;
        var byteUnits = [' H', ' kH', ' MH', ' GH', ' TH', ' PH', ' EH', ' ZH', ' YH' ];
        while (hashrate > 1000){
            hashrate = hashrate / 1000;
            i++;
        }
        return hashrate.toFixed(2) + byteUnits[i];
    }
	
	function getReadableDifficultyString(difficulty, precision){
		if (isNaN(parseFloat(difficulty)) || !isFinite(difficulty)) return 0;
		if (typeof precision === 'undefined') precision = 2;
		var units = ['', 'k', 'M', 'G', 'T', 'P'],
            number = Math.floor(Math.log(difficulty) / Math.log(1000));
		if (units[number] === undefined || units[number] === null) {
            return 0
        }
        return (difficulty / Math.pow(1000, Math.floor(number))).toFixed(precision) + ' ' +  units[number];
    }
	
    function formatBlockLink(hash){
        return '<a href="' + getBlockchainUrl(hash) + '">' + hash + '</a>';
    }

	function formatShortBlockLink(hash){
        return '<a href="' + getBlockchainUrl(hash) + '">' + getShortAddressString(hash) + '</a>';
    }

    function getReadableCoins(coins, digits, withoutSymbol){
        var amount = (parseInt(coins || 0) / coinUnits).toFixed(digits || coinUnits.toString().length - 1);
        return amount + (withoutSymbol ? '' : (' ' + symbol));
    }

	function getTransactionType(tx)
	{
		switch (tx)
		{
			case 0:
				return "No RingCT";
			case 1:
				return "Full RingCT";
			case 2:
				return "Simple RingCT";
			case 3:
				return "Full Bulletproof v1";
			case 4:
				return "Simple Bulletproof v1";
			case 5:
				return "Bulletproof v2";
		}
	}

    function formatDate(time){
        if (!time) return '';
        return new Date(parseInt(time) * 1000).toLocaleString();
    }
	
	function formatBytes(a,b) {
		if(0==a)return"0 B";var c=1024,d=b||2,e=["B","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]
	}
	
	function formatPaymentLink(hash){
        return '<a href="' + getTransactionUrl(hash) + '">' + hash + '</a>';
    }

	function formatShortPaymentLink(hash){
        return '<a href="' + getTransactionUrl(hash) + '">' + getShortAddressString(hash) + '</a>';
    }

    window.onhashchange = function(){
        routePage();
    };

	function getShortAddressString(address) {
        var start = address.substr(0, 6);
        var end = address.substr(address.length - 6);
        return start + "..." + end;
    }

    function fetchLiveStats() {
        $.ajax({
            url: "./api/daemon/get_info/",
            dataType: 'json',
            cache: 'false'
        }).done(function(data, success){
            lastStats = data.result;
			nodeStatus = success;
            if (currentPage) currentPage.update();
			nodeInfo();
        }).always(function () {
			setTimeout(function() {
				fetchLiveStats();
			}, refreshDelay);
        });
    }

    function floatToString(float) {
        return float.toFixed(6).replace(/[0\.]+$/, '');
    }

	function nodeInfo() {
        if(nodeStatus) {
			$('#node_connection').html('Online').addClass('text-success').removeClass('text-danger');
			$('#node_height').html(parseInt(lastStats['height']));
			$('#node_inc').html(parseInt(lastStats['incoming_connections_count']));
			$('#node_out').html(parseInt(lastStats['outgoing_connections_count']));
			if (lastStats['version'] !== 'undefined'){
				$('#node_ver').html(lastStats['version']);
			}
		} else {
			$('#node_connection').html('Offline').addClass('text-danger').removeClass('text-success');
		}
    }
	
    var xhrPageLoading;
    function routePage(loadedCallback) {

		if (currentPage) currentPage.destroy();
        $('#page').html('');
		$('#loading').show();

        if (xhrPageLoading)
            xhrPageLoading.abort();

        $('.hot_link').parent().removeClass('active');
        var $link = $('a.hot_link[href="' + (window.location.hash || '#') + '"]');

        $link.parent().addClass('active');
        var page = $link.data('page');

        xhrPageLoading = $.ajax({
            url: 'pages/' + page,
            cache: false,
            success: function (data) {
                $('#loading').hide();
                $('#page').show().html(data);
				if (currentPage) {
					currentPage.init();
                	currentPage.update();
				}

                if (loadedCallback) loadedCallback();
            }
        });
    }

    function getBlockchainUrl(id) {
        return blockchainExplorer.replace('{id}', id);
	}

	function getinfo() {
		$.ajax({
			url: "./api/daemon/get_info/",
			timeout: 1500 //in milliseconds
		})
		.done(function (data) {
			try {
				lastStats = data.result;
			} catch (e) {
				lastStats = data;
			}
			routePage(fetchLiveStats);
		});
	}
	
    $(function(){
		getinfo();
    });
	
    // Blockexplorer functions
    urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
           return null;
        }
        else{
           return results[1] || 0;
        }
    }

	$(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
	
	function hex2a(hexx) {
		var hex = hexx.toString();//force conversion
		var str = '';
		for (var i = 0; i < hex.length; i += 2)
			str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		return str;
	}

	function alertError(message) {
	$('#page').after(
		'<div class="alert alert-danger alert-dismissable fade in" style="position: fixed; right: 50px; top: 50px;">'+
		'<button id="popup_alert" type="button" class="close" ' + 
		'data-dismiss="alert" aria-hidden="true">' + 
		'&times;' + 
		'</button>' + 
		'<strong>' + message + '</strong><br />' +
		'</div>');
	}

	function alertSuccess(message) {
	$('#page').after(
		'<div class="alert alert-success alert-dismissable fade in" style="position: fixed; right: 50px; top: 50px;">'+
		'<button id="popup_alert" type="button" class="close" ' + 
		'data-dismiss="alert" aria-hidden="true">' + 
		'&times;' + 
		'</button>' + 
		'<strong>' + message + '</strong><br />' +
		'</div>');
	}
</script>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
			</button>
			
        </div>
		
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-left explorer_menu">
				
				<li><a class="hot_link" data-page="miner.php" href="#miner">
                    <i class="fa fa-search"></i> Miner
				</a></li>
				
				<li><a class="hot_link" data-page="wallet.php" href="#wallet">
                    <i class="fa fa-money"></i> Wallet
                </a></li>

			    <li><a class="hot_link" data-page="home.php" href="#">
                    <i class="fa fa-cubes"></i> Explorer
                </a></li>
				
				<li><a class="hot_link" data-page="api.php" href="#api">
                    <i class="fa fa-code"></i> API
				</a></li>
				
				<li><a class="hot_link" data-page="tools.php" href="#tools">
                    <i class="fa fa-wrench"></i> Tools
				</a></li>
			
                <li style="display:none;"><a class="hot_link" data-page="blockchain_block.php" href="#blockchain_block"><i class="fa fa-cubes"></i> Block
                </a></li>

                <li style="display:none;"><a class="hot_link" data-page="blockchain_transaction.php" href="#blockchain_transaction"><i class="fa fa-cubes"></i> Transaction
                </a></li>
				
				<li style="display:none;"><a class="hot_link" data-page="blockchain_payment_id.php" href="#blockchain_payment_id"><i class="fa fa-cubes"></i> Transactions by Payment ID
                </a></li>
            </ul>
		</div>
	  </div>
</div>

<div id="content">
	<div class="container">

		<div id="page"></div>

		<p id="loading" class="text-center"><i class="fa fa-circle-o-notch fa-spin"></i></p>

	</div>
</div>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6">
				<p><small>&copy; 2018 <strong>NERVA</strong></small></p>
				<p><small>&copy; 2017-2018 <strong>Karbo</strong></small></p>
				<ul>
					<li><a href="https://getnerva.org/">Official Website</li>
					<li><a href="https://bitbucket.org/nerva-project">BitBucket</li>
					<li><a href="https://discord.gg/jsdbEns/">Discord</li>
				</ul>
				
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6">
				<p>
					<small>
					<!-- This copyright should be left intact -->
					<a target="_blank" href="https://bitbucket.org/nerva-project/explorer"><i class="fa fa-github"></i> NERVA Blockchain Explorer </a>v0.0.1.0<br />
					<span class="text-muted">Open source and licensed under the <a href="http://www.gnu.org/licenses/gpl-2.0.html">GPL</a><br />
					Based on: 
					<strong><a target="_blank" href="https://github.com/Karbovanets/Karbowanec-Blockchain-Explorer" class="text-muted"> Karbowanec Blockchain Explorer </a>v1.1.3 &<br />
					cryptonote-universal-pool</strong></span>
					</small>
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6">
			
				<strong class="text-info">Node info</strong>
				
				<ul class="text-info">
					<li>Status: <span id="node_connection" class="text-danger">Offline</span></li>
					<li>Version: <span id="node_ver">...</span></li>
					<li>Height: <span id="node_height">...</span></li>
					<li>Incoming P2P connections: <span id="node_inc">...</span></li>
					<li>Outgoing P2P connects: <span id="node_out">...</span></li>
				</ul>

			</div>
		</div>
    </div>
</footer>
<a href="#" class="scrollup"><i class="fa fa-chevron-circle-up"></i></a>
	<script type="text/javascript">
			jQuery(function($) { $(document).ready(function() {
				$(window).scroll(function(){
					if ($(this).scrollTop() > 500) {
						$('.scrollup').fadeIn();
					} else {
						$('.scrollup').fadeOut();
					}
				}); 
		
				$('.scrollup').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 600);
					return false;
				});

				$('.scrollup').css('opacity','0.3');
		 
				$('.scrollup').hover(function(){
					$(this).stop().animate({opacity: 0.9}, 400);
				 }, function(){
					$(this).stop().animate({opacity: 0.3}, 400);
				});  
					
			});});
	</script>	
</body>
</html>
