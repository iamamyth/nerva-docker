#!/bin/bash

IMG_NAME="nerva/node"

if [ ! "$(docker images --format '{{.Repository}}:{{.Tag}}' | grep ${IMG_NAME})" ]; then
	docker pull ${IMG_NAME}
fi

if [ ! "$(docker ps -aq -f name=nerva -f ancestor=${IMG_NAME})" ]; then
	docker run -i --privileged --name "nerva" --publish 17500:80 --publish 17565:17565 --publish 17566:17566 --publish 19566:19566 -t ${IMG_NAME}:latest /bin/bash
else
	docker start nerva
	docker exec -it nerva /bin/bash
fi
